package advinha.timoteojorge.com.br.adivinha;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private Button botaoJogar;
    private TextView resultText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botaoJogar = (Button) findViewById(R.id.botao_jogar_id);
        resultText = (TextView) findViewById(R.id.resultado_id);

        botaoJogar.setOnClickListener(new  View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Random random = new Random();

                int randomNumber = random.nextInt(10);
                resultText.setText("Chosen number: " + randomNumber );
            }
        });
        //textoResultado.setText("Texto Alterado");
    }
}
